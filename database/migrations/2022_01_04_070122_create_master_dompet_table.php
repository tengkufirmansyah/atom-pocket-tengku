<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterDompetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_dompet', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('referensi')->nullable();
            $table->string('deskripsi')->nullable();
            $table->bigInteger('status_id');
            $table->timestamps();
        });

        Schema::create('master_dompet_status', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_dompet');
        Schema::dropIfExists('master_dompet_status');
    }
}
