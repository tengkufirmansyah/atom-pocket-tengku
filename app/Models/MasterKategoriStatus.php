<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterKategoriStatus extends Model
{
    protected $table = 'master_kategori_status';

    protected $primaryKey = 'id';
    protected $fillable = [
        'nama'
    ];
}
