<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterDompet extends Model
{
    protected $table = 'master_dompet';

    protected $primaryKey = 'id';
    protected $fillable = [
        'nama',
        'referensi',
        'deskripsi',
        'status_id'
    ];

    public function status()
    {   
        return $this->belongsTo(MasterDompetStatus::class,'status_id');
    }
}
