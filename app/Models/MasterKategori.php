<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterKategori extends Model
{
    protected $table = 'master_kategori';

    protected $primaryKey = 'id';
    protected $fillable = [
        'nama',
        'deskripsi',
        'status_id'
    ];

    public function status()
    {   
        return $this->belongsTo(MasterKategoriStatus::class,'status_id');
    }
}
