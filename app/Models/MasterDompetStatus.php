<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterDompetStatus extends Model
{
    protected $table = 'master_dompet_status';

    protected $primaryKey = 'id';
    protected $fillable = [
        'nama'
    ];
}
