<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksi';

    protected $primaryKey = 'id';
    protected $fillable = [
        'kode',
        'tanggal',
        'nilai',
        'deskripsi',
        'dompet_id',
        'kategori_id',
        'status_id',
    ];

    public function dompet()
    {   
        return $this->belongsTo(MasterDompet::class,'dompet_id');
    }

    public function kategori()
    {   
        return $this->belongsTo(MasterKategori::class,'kategori_id');
    }

    public function status()
    {   
        return $this->belongsTo(TransaksiStatus::class,'status_id');
    }
}
