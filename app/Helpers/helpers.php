<?php
use App\Models\Transaksi;

if (!function_exists('H_generateCode')) {
    function H_generateCode($status)
    {
        $check = Transaksi::where('status_id','=', $status)->orderBy('id', 'desc');

        if ($check->first() !== null) {
            $code = $check->first()->kode;
            $ex = substr($code,-8,8);
            $num = (int) $ex;
            $nex = sprintf("%08s", $num+1);
            if ($status == 1) {
                $code = 'WIN'.$nex;
            } else {
                $code = 'WOUT'.$nex;
            }
        }else{
            if ($status == 1) {
                $code = 'WIN00000001';
            } else {
                $code = 'WOUT00000001';
            }
        }
        return $code;
    }
}