<?php
namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class LaporanTransaksiExport implements FromView
{
    public function __construct($input)
    {
        $this->input = $input;
    }

    public function view(): View
    {
        return view('laporan.transaksi.proses', $this->input);
    }
}