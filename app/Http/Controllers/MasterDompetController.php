<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\MasterDompet;
use App\Models\MasterDompetStatus;

use DB, Session;

class MasterDompetController extends Controller
{
    public function __construct()
    {
        $this->title = "Master Dompet";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title  = $this->title;
        if ($request->ajax()) {
            $data = MasterDompet::select('*')->with('status');
            if ($request['status']) {
                $data = $data->where('status_id', $request['status']);
            }
            $data = $data->get();
            return response()->json(['data' => $data]);
        }
        return view('master.dompet.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Tambah '.$this->title.' Baru';
        $status = MasterDompetStatus::orderBy('nama','asc')->get();
        $edit = false;
        return view('master.dompet.create',compact('edit','title','status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama' => 'required|min:5',
            'deskripsi' => 'max:100',
        ]);

        DB::beginTransaction();
        try {
            $data = New MasterDompet;
            $data->nama = $request['nama'];
            $data->referensi = $request['referensi'];
            $data->deskripsi = $request['deskripsi'];
            $data->status_id = $request['status'];
            
            $data->save();

            DB::commit();
            
            Session::flash('alert');
            Session::flash('message', 'Data berhasil ter simpan !');
            Session::flash('alert-class', 'success');
            return redirect('/master-dompet');
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('alert');
            Session::flash('message', 'Data gagal ter simpan !');
            Session::flash('alert-class', 'danger');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'View '.$this->title;
        $data = MasterDompet::where('id', $id)->with('status')->first();
        return view('master.dompet.show',compact('title','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $title = 'Edit '.$this->title;
        $status = MasterDompetStatus::get();
        $data = MasterDompet::where('id', $id)->with('status')->first();
        $edit = true;

        return view('master.dompet.edit',compact('edit','title','data','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'nama' => 'required|min:5',
            'deskripsi' => 'max:100',
        ]);
        DB::beginTransaction();
        try {
            $data = MasterDompet::find($id);
            $data->nama = $request['nama'];
            $data->referensi = $request['referensi'];
            $data->deskripsi = $request['deskripsi'];
            $data->status_id = $request['status'];
            
            $data->save();

            DB::commit();
            Session::flash('alert');
            Session::flash('message', 'Data berhasil ter ubah !');
            Session::flash('alert-class', 'success');
            return redirect('/master-dompet');
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('alert');
            Session::flash('message', 'Data gagal ter ubah !');
            Session::flash('alert-class', 'danger');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }

    /**
     * change the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = MasterDompet::find($id);
            if($request['status'] == 'Aktif'){
                $data->status_id = 2;
            }else{
                $data->status_id = 1;
            }
            
            $data->save();

            DB::commit();
            Session::flash('alert');
            Session::flash('message', 'Data berhasil ter ubah !');
            Session::flash('alert-class', 'success');
            return redirect('/master-dompet');
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('alert');
            Session::flash('message', 'Data gagal ter ubah !');
            Session::flash('alert-class', 'danger');
        }
    }
}
