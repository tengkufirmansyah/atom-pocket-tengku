<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Transaksi;
use App\Models\MasterKategori;
use App\Models\MasterDompet;
use App\Models\TransaksiStatus;

use DB, Session;

class DompetMasukController extends Controller
{
    public function __construct()
    {
        $this->title = "Dompet Masuk";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title  = $this->title;
        if ($request->ajax()) {
            $data = Transaksi::select('*')->with('status','kategori','dompet')->where('status_id', 1)->get();
            return response()->json(['data' => $data]);
        }
        return view('transaksi.dompet-masuk.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Tambah '.$this->title.' Baru';
        $status = TransaksiStatus::orderBy('nama','asc')->get();
        $kategori = MasterKategori::orderBy('nama','asc')->where('status_id', 1)->get();
        $dompet = MasterDompet::orderBy('nama','asc')->where('status_id', 1)->get();
        $edit = false;
        return view('transaksi.dompet-masuk.create',compact('edit','title','status','kategori','dompet'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nilai' =>'required|min:1',
            'deskripsi' => 'max:100',
        ]);

        DB::beginTransaction();
        try {
            $data = New Transaksi;
            $data->kode = H_generateCode(1);
            $data->tanggal = date('Y-m-d');
            $data->nilai = $request['nilai'];
            $data->deskripsi = $request['deskripsi'];
            $data->dompet_id = $request['dompet_id'];
            $data->kategori_id = $request['kategori_id'];
            $data->status_id = 1;
            $data->save();

            DB::commit();
            
            Session::flash('alert');
            Session::flash('message', 'Data berhasil ter simpan !');
            Session::flash('alert-class', 'success');
            return redirect('/dompet-masuk');
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('alert');
            Session::flash('message', 'Data gagal ter simpan !');
            Session::flash('alert-class', 'danger');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'View '.$this->title;
        $data = Transaksi::where('id', $id)->with('status')->first();
        return view('transaksi.dompet-masuk.show',compact('title','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $title = 'Edit '.$this->title;
        $status = TransaksiStatus::get();
        $data = Transaksi::where('id', $id)->with('status')->first();
        $edit = true;

        return view('transaksi.dompet-masuk.edit',compact('edit','title','data','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'nama' => 'required|min:5',
            'deskripsi' => 'max:100',
        ]);
        DB::beginTransaction();
        try {
            $data = Transaksi::find($id);
            $data->nama = $request['nama'];
            $data->deskripsi = $request['deskripsi'];
            $data->status_id = $request['status'];
            
            $data->save();

            DB::commit();
            Session::flash('alert');
            Session::flash('message', 'Data berhasil ter ubah !');
            Session::flash('alert-class', 'success');
            return redirect('/dompet-masuk');
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('alert');
            Session::flash('message', 'Data gagal ter ubah !');
            Session::flash('alert-class', 'danger');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }

}
