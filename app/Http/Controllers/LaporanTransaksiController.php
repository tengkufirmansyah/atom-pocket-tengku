<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Transaksi;
use App\Models\MasterKategori;
use App\Models\MasterDompet;
use App\Models\TransaksiStatus;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\LaporanTransaksiExport;

use DB, Session;

class LaporanTransaksiController extends Controller
{
    public function __construct()
    {
        $this->title = "Laporan Transaksi";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $this->title;
        $req = $request->all();
        if (isset($req['tanggal_awal'])) {
            if ($req['tanggal_awal'] > $req['tanggal_akhir']) {
                Session::flash('alert');
                Session::flash('message', 'Tanggal awal tidak boleh melebihi tanggal akhir !');
                Session::flash('alert-class', 'danger');
                return back();
            }
        }
        $link = str_replace(['{','}','"'], '',json_encode($req));
        $link = str_replace(':', '=',$link);
        $link = str_replace(',', '&',$link);
        $kategori = MasterKategori::orderBy('nama','asc')->where('status_id', 1)->get();
        $dompet = MasterDompet::orderBy('nama','asc')->where('status_id', 1)->get();
        return view('laporan.transaksi.index', compact('title', 'req', 'kategori', 'dompet','link'));
    }

    public function view(Request $request)
    {
        $data = $this->getData($request);
        return view('laporan.transaksi.proses', compact('data', 'request'));
    }

    public function exportExcel(Request $request) 
    {
        $data = $this->getData($request);
        $input = [
            'data' => $data,
            'request' => $request
        ];
        return Excel::download(new LaporanTransaksiExport($input), 'Laporan Transaksi.xlsx');
    }

    public function getData($request)
    {
        $arr = [];
        if (isset($request['uang_masuk'])) {
            $arr[] = 1;
        }
        if (isset($request['uang_keluar'])) {
            $arr[] = 2;
        }
        $data = Transaksi::select('*')->with('status','kategori','dompet')
                        ->whereBetween('tanggal', [$request['tanggal_awal'], $request['tanggal_akhir']])
                        ->whereIn('status_id', $arr);
        if ($request['kategori_id'] != 'all') {
            $data = $data->where('kategori_id', $request['kategori_id']);
        }
        if ($request['dompet_id'] != 'all') {
            $data = $data->where('dompet_id', $request['dompet_id']);
        }
        $data = $data->orderBy('id', 'asc')->get();

        return $data;
    }
}
