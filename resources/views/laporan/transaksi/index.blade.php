@extends('layouts.template')
@section('title',$title)
@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{$title}}</h1>
</div>

<div class="row">
    <!-- Content Column -->
    <div class="col-lg-12 mb-6">
        <!-- Project Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">View</h6>
            </div>
            <div class="card-body">
                <form id="form" method="GET">
                    <div class="card-body">
                        <div class="validation-message"></div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="tanggal_awal" class="control-label">{{ 'Tanggal Awal' }} *</label>
                                    <input class="form-control" name="tanggal_awal" type="date" id="tanggal_awal" value="{{ isset($req['tanggal_awal']) ? $req['tanggal_awal'] : date('Y-m-d')}}" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="tanggal_akhir" class="control-label">{{ 'Tanggal Akhir' }} *</label>
                                    <input class="form-control" name="tanggal_akhir" type="date" id="tanggal_akhir" value="{{ isset($req['tanggal_akhir']) ? $req['tanggal_akhir'] : date('Y-m-d')}}" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="tanggal_akhir" class="control-label">{{ 'Status :' }}</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="uang_masuk" id="uang_masuk" {{ isset($req['uang_masuk']) ? 'checked' : ''}}>
                                    <label class="form-check-label" for="uang_masuk">
                                        Tampilkan Uanga Masuk
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="uang_keluar" id="uang_keluar" {{ isset($req['uang_keluar']) ? 'checked' : ''}}>
                                    <label class="form-check-label" for="uang_keluar">
                                        Tampilkan Uanga Keluar
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="kategori_id" class="control-label">{{ 'Kategori' }} *</label>
                                    <select class="form-control" name="kategori_id" id="kategori_id" required>
                                        <option value="all">Semua</option>
                                        @foreach($kategori as $val)
                                        <option value="{{$val->id}}" {{ isset($req['kategori_id']) ? $req['kategori_id'] == $val->id ? 'selected' : '' : ''}}>{{$val->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="dompet" class="control-label">{{ 'Dompet' }} *</label>
                                    <select class="form-control" name="dompet_id" id="dompet_id" required>
                                        <option value="all">Semua</option>
                                        @foreach($dompet as $val)
                                        <option value="{{$val->id}}" {{ isset($req['dompet_id']) ? $req['dompet_id'] == $val->id ? 'selected' : '' : ''}}>{{$val->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button type="submit" id="submit" class="btn btn-primary mr-2">Proses</button>
                    </div>
                    <div class="card-footer">
                        @if(isset($req['tanggal_awal']))
                        <a href="{{url('laporan-transaksi/export-excel?').$link}}" target="_blank" class="btn btn-success">Excel</a>
                        <iframe width="100%" style="border: 1px solid #ddd; margin-top: 10px" height="500px" src="{{url('laporan-transaksi/view?').$link}}">Your browser isn't compatible</iframe>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection