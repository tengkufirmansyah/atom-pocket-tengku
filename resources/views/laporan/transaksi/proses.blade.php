<style>
body{
	color: #333;
	font-family: sans-serif;
}
.table, .table td, .table th {
	border: 1px solid #ddd;
	padding: 15px 20px;
	font-size: 12px;
	color: #333;
}
.table th{
	color: #888;
	text-transform: uppercase;
}
.table {
	width: 100%;
	border-collapse: collapse;
}
.text-right{
	text-align: right;
}
.text-center{
	text-align: center;
}
</style>
<table border="0" width="100%">
	<tr>
		<th colspan="7"><h2><b>RIWAYAT TRANSAKSI</b></h2></th>
	</tr>
	<tr>
		<th colspan="7"><h4><b>{{$request['tanggal_awal']}} - {{$request['tanggal_akhir']}}</b></h4></th>
	</tr>
</table>
<table class="table">
	<thead>
		<tr>
			<th width="1px"><b>#</b></th>
			<th><b>TANGGAL</b></th>
			<th><b>KODE</b></th>
			<th><b>DESKRIPSI</b></th>
			<th><b>DOMPET</b></th>
			<th><b>KATEGORI</b></th>
			<th><b>NILAI</b></th>
		</tr>
	</thead>
	<tbody>
		<?php
			$total_masuk = 0;
			$total_keluar = 0;
		?>
		@foreach($data as $key => $val)
		<?php
			if ($val->nilai < 0) {
				$total_keluar = $total_keluar + $val->nilai;
			}else{
				$total_masuk = $total_masuk + $val->nilai;
			}
		?>
		<tr>
			<td class="text-center">{{$key+1}}</td>
			<td>{{$val->tanggal}}</td>
			<td>{{$val->kode}}</td>
			<td>{{$val->deskripsi}}</td>
			<td>{{$val->dompet->nama}}</td>
			<td>{{$val->kategori->nama}}</td>
			<td class="text-right">{{number_format($val->nilai)}}</td>
		</tr>
		@endforeach
		<tr>
			<td colspan="5"></td>
			<td><b>Total Uang Masuk</b></td>
			<td class="text-right"><b>{{number_format($total_masuk)}}</b></td>
		</tr>
		<tr>
			<td colspan="5"></td>
			<td><b>Total Uang Keluar</b></td>
			<td class="text-right"><b>{{number_format(str_replace('-','', $total_keluar))}}</b></td>
		</tr>
		<tr>
			<td colspan="5"></td>
			<td><b>Total</b></td>
			<td class="text-right"><b>{{number_format($total_masuk + $total_keluar)}}</b></td>
		</tr>
	</tbody>
</table>