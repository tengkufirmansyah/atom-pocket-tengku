@extends('layouts.template')
@section('title',$title)
@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{$title}}</h1>
    <div>
        <a href="{{url(Request::url())}}/create" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Add new</a>
        <select id="filter" class="">
            <option value="">All</option>
            <option value="1">Aktif</option>
            <option value="2">Tidak Aktif</option>
        </select>
    </div>
</div>

<div class="row">
    <!-- Content Column -->
    <div class="col-lg-12 mb-6">
        <!-- Project Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List</h6>
            </div>
            <div class="card-body">
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>NAMA</th>
                            <th>REFERENSI</th>
                            <th>DESKRIPSI</th>
                            <th>STATUS</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        var tbl = $('#example').DataTable( {
            "ajax": "{{url()->full()}}",
            "columns": [
            {
                sortable: false,
                width : 70,
                autoHide: false,
                overflow: 'visible',
                "render": function(data, type, row) {
                        if (row.status.nama == 'Aktif') {
                            return '\
                                <div class="dropdown mb-4">\
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                        Aksi\
                                    </button>\
                                    <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">\
                                        <a class="dropdown-item" href="{{ url("master-dompet") }}/'+row.id+'">View</a>\
                                        <a class="dropdown-item" href="{{ url("master-dompet") }}/'+row.id+'/edit">Ubah</a>\
                                        <a class="dropdown-item" href="{{ url("master-dompet/change") }}/'+row.id+'?status='+row.status.nama+'">Tidak Aktif</a>\
                                    </div>\
                                </div>\
                            ';
                        }else{
                            return '\
                                <div class="dropdown mb-4">\
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                        Aksi\
                                    </button>\
                                    <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">\
                                        <a class="dropdown-item" href="{{ url("master-dompet") }}/'+row.id+'">View</a>\
                                        <a class="dropdown-item" href="{{ url("master-dompet") }}/'+row.id+'/edit">Ubah</a>\
                                        <a class="dropdown-item" href="{{ url("master-dompet/change") }}/'+row.id+'?status='+row.status.nama+'">Aktif</a>\
                                    </div>\
                                </div>\
                            ';
                        }
                },
            },
            { 
                "data": "nama" 
            },
            { 
                "data": "referensi" 
            },
            { 
                "data": "deskripsi" 
            },
            {
                "data": "status_id",
                "render": function (data, type, row) {
                    return row.status.nama;
                }
            }
            ]
        } );
        
        $('#filter').on('change', function(){
            console.log('ada');
            tbl.ajax.url("{{url()->full()}}?status="+$(this).val()).load();
        });
    } );
</script>
@endsection