<div class="form-group">
    <label for="nama" class="control-label">{{ 'Nama' }} *</label>
    <input class="form-control" name="nama" type="text" id="nama" value="{{ isset($data->nama) ? $data->nama : ''}}" required minlength="5">
</div>
<div class="form-group">
    <label for="deskripsi" class="control-label">{{ 'Deskripsi' }}</label>
    <textarea class="form-control" name="deskripsi" maxlength="100">{{ isset($data->deskripsi) ? $data->deskripsi : ''}}</textarea>
</div>
<div class="form-group">
    <label for="status" class="control-label">{{ 'Status' }} *</label>
    <select class="form-control" name="status" id="status" required>
        @foreach($status as $val)
        <option value="{{$val->id}}" {{ isset($data->status_id) ? $data->status_id == $val->id ? 'selected' : '' : ''}}>{{$val->nama}}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Ubah' : 'Simpan' }}">
</div>
