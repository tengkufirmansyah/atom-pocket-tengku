@extends('layouts.template')
@section('title',$title)
@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{$title}}</h1>
    <div>
        <a href="{{url(Request::url())}}/create" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Add new</a>
    </div>
</div>

<div class="row">
    <!-- Content Column -->
    <div class="col-lg-12 mb-6">
        <!-- Project Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List</h6>
            </div>
            <div class="card-body">
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>TANGGAL</th>
                            <th>KODE</th>
                            <th>DESKRIPSI</th>
                            <th>KATEGORI</th>
                            <th>NILAI</th>
                            <th>DOMPET</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        var tbl = $('#example').DataTable( {
            "ajax": "{{url()->full()}}",
            "columns": [
            { 
                "data": "tanggal" 
            },
            { 
                "data": "kode" 
            },
            { 
                "data": "deskripsi" 
            },
            {
                "data": "kategori_id",
                "render": function (data, type, row) {
                    return row.kategori.nama;
                }
            },
            {
                "data": "nilai",
                "render": function (data, type, row) {
                    return '(+) '+row.nilai.toLocaleString();
                }
            },
            {
                "data": "dompet_id",
                "render": function (data, type, row) {
                    return row.dompet.nama;
                }
            }
            ]
        } );
    } );
</script>
@endsection