<div class="row">
    <div class="col-3">
        <div class="form-group">
            <label for="kode" class="control-label">{{ 'Kode' }} *</label>
            <input class="form-control" name="kode" type="text" id="kode" value="{{ isset($data->kode) ? $data->kode : H_generateCode(2)}}" required readonly>
        </div>
    </div>
    <div class="col-3">
        <div class="form-group">
            <label for="tanggal" class="control-label">{{ 'Tanggal' }} *</label>
            <input class="form-control" name="tanggal" type="date" id="tanggal" value="{{ isset($data->tanggal) ? $data->tanggal : date('Y-m-d')}}" required readonly>
        </div>
    </div>
    <div class="col-3">
        <div class="form-group">
            <label for="kategori_id" class="control-label">{{ 'Kategori' }} *</label>
            <select class="form-control" name="kategori_id" id="kategori_id" required>
                @foreach($kategori as $val)
                <option value="{{$val->id}}" {{ isset($data->kategori_id) ? $data->kategori_id == $val->id ? 'selected' : '' : ''}}>{{$val->nama}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-3">
        <div class="form-group">
            <label for="dompet" class="control-label">{{ 'Dompet' }} *</label>
            <select class="form-control" name="dompet_id" id="dompet_id" required>
                @foreach($dompet as $val)
                <option value="{{$val->id}}" {{ isset($data->dompet_id) ? $data->dompet_id == $val->id ? 'selected' : '' : ''}}>{{$val->nama}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="nilai" class="control-label">{{ 'Nilai' }} *</label>
            <input class="form-control" name="nilai" type="number" id="nilai" value="{{ isset($data->nilai) ? $data->nilai : ''}}" required min="0">
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="deskripsi" class="control-label">{{ 'Deskripsi' }}</label>
            <textarea class="form-control" name="deskripsi" maxlength="100">{{ isset($data->deskripsi) ? $data->deskripsi : ''}}</textarea>
        </div>
    </div>
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Ubah' : 'Simpan' }}">
</div>
