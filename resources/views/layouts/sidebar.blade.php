<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-text mx-3">Dompet Tengku</div>
    </a>
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Master
    </div>

    <li class="nav-item">
        <a class="nav-link" href="{{url('master-dompet')}}">
            <i class="fas fa-fw fa-wallet"></i>
            <span>Dompet</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('master-kategori')}}">
            <i class="fas fa-fw fa-list"></i>
            <span>Kategori</span>
        </a>
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
        Transaksi
    </div>

    <li class="nav-item">
        <a class="nav-link" href="{{url('dompet-masuk')}}">
            <i class="fas fa-fw fa-sign-in-alt"></i>
            <span>Dompet Masuk</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{url('dompet-keluar')}}">
            <i class="fas fa-fw fa-sign-out-alt"></i>
            <span>Dompet Keluar</span>
        </a>
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
        Laporan
    </div>

    <li class="nav-item">
        <a class="nav-link" href="{{url('laporan-transaksi')}}">
            <i class="fas fa-fw fa-clipboard-list"></i>
            <span>Transaksi</span>
        </a>
    </li>
</ul>