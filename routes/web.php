<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MasterDompetController;
use App\Http\Controllers\MasterKategoriController;
use App\Http\Controllers\DompetMasukController;
use App\Http\Controllers\DompetKeluarController;
use App\Http\Controllers\LaporanTransaksiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/master-dompet',MasterDompetController::class);
Route::get('/master-dompet/change/{id}',[MasterDompetController::class,'change'])->name('master-dompet.change');

Route::resource('/master-kategori',MasterKategoriController::class);
Route::get('/master-kategori/change/{id}',[MasterKategoriController::class,'change'])->name('master-kategori.change');

Route::resource('/dompet-masuk',DompetMasukController::class);

Route::resource('/dompet-keluar',DompetKeluarController::class);


Route::get('laporan-transaksi', [LaporanTransaksiController::class,'index']);
Route::get('laporan-transaksi/view', [LaporanTransaksiController::class,'view']);
Route::get('laporan-transaksi/export-excel', [LaporanTransaksiController::class,'exportExcel'])->name('laporan-transaksi.export-excel');
